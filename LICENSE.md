﻿# OGN presentation license

Feel free to use the presentation or parts of it.  Please give
due credit.  Please share your derivative work (presentation)
like this presentation is shared.

Image credits:
* Coverage maps for stations: Melissa Jenkins
* Coverage map Europe Europa: Michal Krupicka (August 2015)
* OGN-Logo: Frank Schellenberg
* Background: Gerhard Wesp (Chile 2010)
* Spectrogram: Pawel Jalocha, Gerhard Wesp
* System architecture: Wojtek Buczak, Gerhard Wesp
* Screenshots: The respective page creators
* Installations: Pawel Jalocha and others
* Other images (Raspberry fotos etc.): Gerhard Wesp
